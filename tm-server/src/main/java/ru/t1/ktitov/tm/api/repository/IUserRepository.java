package ru.t1.ktitov.tm.api.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User update(@Nullable User user);

    User findByLogin(String login);

    User findByEmail(String email);

    Boolean isLoginExist(String login);

    Boolean isEmailExist(String email);

    void delete(@Nullable String id);

}
