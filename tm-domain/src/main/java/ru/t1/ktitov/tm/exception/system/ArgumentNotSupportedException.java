package ru.t1.ktitov.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Not supported argument.");
    }

    public ArgumentNotSupportedException(@NotNull final String argument) {
        super("Error! Not supported argument: " + argument + ".");
    }

}
